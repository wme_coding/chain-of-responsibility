import logger.ConsoleLogger;
import logger.ErrorLogger;
import logger.FileLogger;
import logger.Logger;
import org.junit.Test;

public class TestLogger {

    @Test
    public void testLogger(){
        Logger errorLogger = new ErrorLogger(Logger.ERROR);
        Logger fileLogger = new FileLogger(Logger.DEBUG);
        Logger consoleLogger = new ConsoleLogger(Logger.INFO);
        errorLogger.setNextLogger(fileLogger);
        fileLogger.setNextLogger(consoleLogger);

        Logger loggerChain = errorLogger;

        loggerChain.logMessage(Logger.INFO, "Information.");
        loggerChain.logMessage(Logger.DEBUG, "Debug");
        loggerChain.logMessage(Logger.ERROR, "Error");
    }
}
